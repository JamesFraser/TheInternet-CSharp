# The-Internet Selenium Framework

A simple framework for running Selenium tests against The-Internet: https://the-internet.herokuapp.com/
Tests can be run using either NUnit or SpecFlow BDD-style feature files.

## Getting Started

Once you've downloaded the solution repository, you may see reference errors (such as SpecFlow) - typically building the solution will fix this.

The framework is split into three projects: Core, Model and Tests.  

### Core
Used to handle the logic of the framework - creating/tearing down the Driver, handling config and keeping the SpecFlow step files standardised.  

### Model
Used to represent the pages and controls found in the site and handling how the driver can interact with them. These are typically split into directories for each page or a common directory if a control appears on multiple pages.  

### Tests
Where the actual tests should be placed. In this repo, both NUnit files and SpecFlow feature files are in this directory to illustrate how they can be used.  

## Model

### Controls
Used to represent any objects that are typically repeated (such as shopping catalogue items) or complex (such as search controls) in order to encapsulate their logic and keep the model clean and simple to use. 

In order to create a control object, you must extend the `ControlBase` class in order to provide access to the driver (any common functionality such as IsVisible(), etc can be added here). The constructor should take in an IWebDriver object and, in the case of repeated controls, an IWebElement container object. The container is used to restrict the driver to the current control when finding elements. 

### Pages
In order to create a page object, you must extend the `PageBase` class in order to provide access to the driver (and access convenience methods such as returning the page title, url, etc). The constructor should take in an IWebDriver object. This repository follows the convention that elements should be kept private and methods should be created to interact with them (such as `Login(string username, string password)`) but this is not a hard rule.

If a new page is created it should be added to the `Core/Support/BaseTestCase.cs` and/or `Core/Steps/StepsSupport/BaseStepDefinition.cs` classes so it can be used easily by NUnit and Specflow tests respectively.

### Locators
Keeping all page/control locators in an internal class keeps refactoring in the future simple.

## Tests

### Changing Test Settings
Tests can be run in a variety of ways, this framework currently only supports local runs on Firefox, but it would be simple to add additional browser types or to run on SauceLabs or a Selenium Grid etc. To make use of these (if implemented), go to the `App.config` file in the `Tests` project and update this section

```
<appSettings>  
	<add key="TestExecutionEnvironment" value="local" />  
	<add key="BrowserChoice" value="firefox" />  
</appSettings>
```
to use the correct values. The keys can also be set using environment variables for use in pipeline deployments (NOTE: environment variables are given priority over the config file).

### Creating Tests
#### NUnit
To add a new set of NUnit tests, you must extend the `BaseTestCase` class, which gives you access to the driver and page/control objects. Add a Setup() method which creates the driver and opens the relevant page for testing.
```
[SetUp]
protected void Setup()
{
	Driver = WebDriverFactory.Get();
	LoginPage.Open();
}
```
Add a TearDown() method to handle quitting the browser.
```
[TearDown]
protected void Teardown()
{
	Driver.Quit();
}
```
Tests can then be created using the Page Object Model accessible by the base class.
```
[Test]
public void Test_LoginSucceeds_WithValidCredentials()
{
	var loginSuccessful = LoginPage.Login("username", "password");
	Assert.IsTrue(loginSuccessful, "Login with valid credentials failed.");
}
```

#### SpecFlow
SpecFlow documentation available: http://specflow.org/getting-started/

Steps should be added in a similar fashion to NUnit above, but the steps files should be added to `Core/Steps` and should extend the `BaseStepDefinition` class. No setup/teardown is necessary as the `ScenarioHooks` should handle that automatically for each test.
```
[Binding]
public sealed class LoginPageSteps : BaseStepDefinition
{
	[Given(@"I am on the login page")]
	public void GivenIAmOnTheLoginPage()
	{
		LoginPage.Open();
	}
}
```
Feature files should be added in the `Tests/Features` directory (ideally broken into further directories based on page or element under test).

### Running tests
NUnit tests can be run in the standard way either from the IDE or command line.
SpecFlow can be customised using the `Tests/Default.srprofile` file, allowing you to customise settings, such as the number of concurrent tests to run, abandon-run conditions, etc.
SpecFlow documentation available: http://specflow.org/getting-started/
