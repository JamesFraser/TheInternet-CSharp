﻿using System;
using Core.Configs;
using Core.Enums;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Core.DriverSupport
{
    public class WebDriverFactory
    {
        public static IWebDriver Get()
        {
            var browser = TestConfig.BrowserChoice;
            var environment = TestConfig.TestExecutionEnvironment;
            switch (environment)
            {
                case TestExecutionEnvironment.Local:
                    return GetLocalDriver(browser);
                default:
                    throw new ArgumentException(
                        "Test execution environment '" + environment + "' not recognised.");
            }
        }

        private static IWebDriver GetLocalDriver(Browser browser)
        {
            IWebDriver driver;
            switch (browser)
            {
                case Browser.Firefox:
                    driver = new FirefoxDriver();
                    break;
                default:
                    throw new ArgumentException(
                        "Unrecognised browser choice '" + browser + "' when initialising driver for local environment.");
            }

            driver.Manage().Window.Maximize();
            driver.Manage().Cookies.DeleteAllCookies();

            return driver;
        }
    }
}
