﻿using Model.Pages;
using OpenQA.Selenium;

namespace Core.Support
{
    public abstract class BaseTestCase 
    {
        protected static IWebDriver Driver;

        public LoginPage LoginPage
        {
            get { return new LoginPage(Driver); }
        }

        public SecureAreaPage SecureAreaPage
        {
            get { return new SecureAreaPage(Driver); }
        }

        public HoversPage HoversPage
        {
            get { return new HoversPage(Driver); }
        }
    }
}
