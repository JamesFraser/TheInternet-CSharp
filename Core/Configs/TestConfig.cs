﻿using System;
using System.Configuration;
using Core.Enums;

namespace Core.Configs
{
    public class TestConfig
    {
        private static string GetSetting(string key)
        {
            return Environment.GetEnvironmentVariable(key) ?? ConfigurationManager.AppSettings[key];
        }

        internal static TestExecutionEnvironment TestExecutionEnvironment
        {
            get
            {
                var environment = GetSetting("TestExecutionEnvironment");

                switch (environment.ToLower())
                {
                    case "local":
                        return TestExecutionEnvironment.Local;
                    default:
                        throw new ConfigurationErrorsException(
                            $"'{environment}' was not recognised as an execution environment. Try one of the following values: {string.Join(", ", Enum.GetNames(typeof(TestExecutionEnvironment)))}");
                }
            }
        }

        internal static Browser BrowserChoice
        {
            get
            {
                var browser = GetSetting("BrowserChoice");

                switch (browser.ToLower())
                {
                    case "firefox":
                        return Browser.Firefox;
                    default:
                        throw new ConfigurationErrorsException(
                            $"'{browser}' was not recognised as a browser choice. Try one of the following values: {string.Join(", ", Enum.GetNames(typeof(Browser)))}");
                }
            }
        }
    }
}
