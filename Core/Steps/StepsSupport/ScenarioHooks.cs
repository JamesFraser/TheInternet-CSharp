﻿using Core.DriverSupport;
using TechTalk.SpecFlow;

namespace Core.Steps.StepsSupport
{
    [Binding]
    class ScenarioHooks : BaseStepDefinition
    {
        [BeforeScenario]
        private void BeforeScenario()
        {
            Driver = WebDriverFactory.Get();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            Driver.Quit();
        }
    }
}
    