﻿using Model.Pages;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Core.Steps.StepsSupport
{
    [Binding]
    public class BaseStepDefinition : TechTalk.SpecFlow.Steps
    {
        protected static IWebDriver Driver;

        public LoginPage LoginPage
        {
            get { return new LoginPage(Driver); }
        }

        public SecureAreaPage SecureAreaPage
        {
            get { return new SecureAreaPage(Driver); }
        }

        public HoversPage HoversPage
        {
            get { return new HoversPage(Driver); }
        }
    }
}
