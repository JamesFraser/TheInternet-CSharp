﻿using Core.Steps.StepsSupport;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Core.Steps.LoginPageSteps
{
    [Binding]
    public sealed class LoginPageSteps : BaseStepDefinition
    {
        [Given(@"I am on the login page")]
        public void GivenIAmOnTheLoginPage()
        {
            LoginPage.Open();
        }

        [When(@"I try to log in with the username '(.*)' and password '(.*)'")]
        public void WhenITryToLogInWithTheUsernameAndPassword(string username, string password)
        {
            LoginPage.Login(username, password);
        }

        [Then(@"I am able to log in successfully")]
        public void ThenIAmAbleToLogInSuccessfully()
        {
            Assert.That(SecureAreaPage.LoginSuccessful,
                "Login failed unexpectedly.");
        }

        [Then(@"I am unable to log in")]
        public void ThenIAmUnableToLogIn()
        {
            Assert.That(LoginPage.LoginFailed,
                "Login was successful unexpectedly.");
        }

    }
}
