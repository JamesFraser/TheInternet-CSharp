﻿using Model.Bases;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Model.Controls.HoversPage
{
    public class Figure : ControlBase
    {
        private IWebElement _container;

        public Figure(IWebDriver driver, IWebElement container) : base(driver)
        {
            _container = container;
        }

        private void Hover()
        {
            new Actions(Driver).MoveToElement(_container).Perform();
        }

        private IWebElement NameLabel
        {
            get
            {
                Hover();
                return _container.FindElement(Locators.NameLabel);
            }
        }

        private IWebElement ViewProfileAnchor
        {
            get
            {
                Hover();
                return _container.FindElement(Locators.ViewProfileAnchor);
            }
        }

        public string Name
        {
            get
            {
                return NameLabel.Text.Remove(0, 6);
            }
        }

        public void GoToProfile()
        {
            ViewProfileAnchor.Click();
        }

        private static class Locators
        {
            public static readonly By NameLabel = By.CssSelector("h5");
            public static readonly By ViewProfileAnchor = By.CssSelector("a");
        }
    }
}
