﻿using OpenQA.Selenium;

namespace Model.Bases
{
    public abstract class ControlBase : TestObjectBase
    {
        protected ControlBase(IWebDriver driver) : base(driver)
        {
        }
    }
}
