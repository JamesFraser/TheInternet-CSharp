﻿using OpenQA.Selenium;

namespace Model.Bases
{
    public abstract class PageBase : TestObjectBase
    {
        protected PageBase(IWebDriver driver) : base(driver)
        {
        }
        
        public string Title
        {
            get { return Driver.Title; }
        }
        
        public string Url
        {
            get { return Driver.Url; }
        }
        
        public string Source
        {
            get { return Driver.PageSource; }
        }
        
        public abstract bool IsLoaded();
    }
}
