﻿using OpenQA.Selenium;

namespace Model.Bases
{
    public abstract class TestObjectBase
    {
        public IWebDriver Driver;

        protected TestObjectBase(IWebDriver driver)
        {
            Driver = driver;
        }
    }
}
