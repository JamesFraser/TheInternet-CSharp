﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Model.Bases;
using Model.Controls.HoversPage;
using OpenQA.Selenium;

namespace Model.Pages
{
    public class HoversPage : PageBase
    {
        public HoversPage(IWebDriver driver) : base(driver)
        {
        }

        public ReadOnlyCollection<Figure> Figures
        {
            get
            {
                var containers = Driver.FindElements(Locators.Figures);
                var figures = new List<Figure>();
                foreach (var container in containers)
                {
                    figures.Add(new Figure(Driver, container));
                }
                return figures.AsReadOnly();
            }
        }

        public void Open()
        {
            Driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/hovers");
        }
        
        public override bool IsLoaded()
        {
            return Driver.FindElements(Locators.Figures).Any();
        }

        private static class Locators
        {
            public static readonly By Figures = By.CssSelector(".figure");
        }
    }
}
