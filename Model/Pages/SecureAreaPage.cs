﻿using System.Linq;
using Model.Bases;
using OpenQA.Selenium;

namespace Model.Pages
{
    public class SecureAreaPage : PageBase
    {
        public SecureAreaPage(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement LogoutButton
        {
            get { return Driver.FindElement(Locators.LogoutButton); }
        }
        
        public bool LoginSuccessful
        {
            get { return Driver.FindElements(Locators.LoginSuccessful).Any(); }
        }

        public void Logout()
        {
            LogoutButton.Click();
        }

        public override bool IsLoaded()
        {
            return Driver.FindElements(Locators.LogoutButton).Any();
        }

        private static class Locators
        {
            public static readonly By LogoutButton = By.CssSelector(".example>a");
            public static readonly By LoginSuccessful = By.CssSelector("#flash.flash.success");
        }
    }
}
