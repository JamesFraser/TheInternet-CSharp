﻿using System.Linq;
using Model.Bases;
using OpenQA.Selenium;

namespace Model.Pages
{
    public class LoginPage : PageBase
    {
        public LoginPage(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement UsernameInputbox
        {
            get { return Driver.FindElement(Locators.UsernameInputbox); }
        }

        private IWebElement PasswordInputbox
        {
            get { return Driver.FindElement(Locators.PasswordInputbox); }
        }

        private IWebElement LoginButton
        {
            get { return Driver.FindElement(Locators.LoginButton); }
        }

        public bool LoginFailed
        {
            get { return Driver.FindElements(Locators.LoginFailed).Any(); }
        }

        public void Open()
        {
            Driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/login");
        }

        public bool Login(string username, string password)
        {
            UsernameInputbox.Clear();
            UsernameInputbox.SendKeys(username);
            PasswordInputbox.Clear();
            PasswordInputbox.SendKeys(password);
            LoginButton.Click();

            return !LoginFailed;
        }

        public override bool IsLoaded()
        {
            return Driver.FindElements(Locators.UsernameInputbox).Any();
        }

        private static class Locators
        {
            public static readonly By UsernameInputbox = By.Id("username");
            public static readonly By PasswordInputbox = By.Id("password");
            public static readonly By LoginButton = By.CssSelector("button[type='submit']");
            public static readonly By LoginFailed = By.CssSelector("#flash.flash.error");
        }
    }
}
