﻿using Core.DriverSupport;
using Core.Support;
using NUnit.Framework;

namespace Tests.LoginPage
{
    [TestFixture]
    public class LoginPageTests : BaseTestCase
    {
        private const string ValidUsername = "tomsmith";
        private const string ValidPassword = "SuperSecretPassword!";

        [SetUp]
        protected void Setup()
        {
            Driver = WebDriverFactory.Get();
            LoginPage.Open();
        }

        [TearDown]
        protected void Teardown()
        {
            Driver.Quit();
        }

        [Test]
        public void Test_LoginSucceeds_WithValidCredentials()
        {
            Assert.IsTrue(LoginPage.Login(ValidUsername, ValidPassword),
                "Login with valid credentials failed.");
        }

        [Test]
        public void Test_LoginFails_WithInvalidUsername()
        {
            Assert.IsFalse(LoginPage.Login("invalid", ValidPassword),
                "Login with invalid credentials was successful.");
        }

        [Test]
        public void Test_LoginFails_WithInvalidPassword()
        {
            Assert.IsFalse(LoginPage.Login(ValidUsername, "invalid"),
                "Login with invalid credentials was successful.");
        }
    }
}
