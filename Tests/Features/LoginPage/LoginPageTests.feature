﻿@loginPage
Feature: LoginPageTests

Scenario: I can log in successfully with correct credentials
	Given I am on the login page
	When I try to log in with the username 'tomsmith' and password 'SuperSecretPassword!'
	Then I am able to log in successfully

Scenario: I can not log in with incorrect username
	Given I am on the login page
	When I try to log in with the username 'nottomsmith' and password 'SuperSecretPassword!'
	Then I am unable to log in

Scenario: I can not log in with incorrect password
	Given I am on the login page
	When I try to log in with the username 'tomsmith' and password 'NotSuperSecretPassword!'
	Then I am unable to log in
