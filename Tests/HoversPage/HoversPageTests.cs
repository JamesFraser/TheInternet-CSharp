﻿using Core.DriverSupport;
using Core.Support;
using NUnit.Framework;

namespace Tests.HoversPage
{
    [TestFixture]
    public class HoversPageTests : BaseTestCase
    {
        [SetUp]
        protected void Setup()
        {
            Driver = WebDriverFactory.Get();
            HoversPage.Open();
        }
        [TearDown]
        protected void Teardown()
        {
            Driver.Quit();
        }

        [Test]
        public void Test_ExpectedNumberOfFigures()
        {
            Assert.AreEqual(3, HoversPage.Figures.Count,
                "There were not 3 figures shown as expected.");
        }
        
        [Test]
        public void Test_ExpectedUserName()
        {
            Assert.AreEqual("user1", HoversPage.Figures[0].Name,
                "The first figure did not have the correct username.");
        }
    }
}
